/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.explorador.controlador;

import com.explorador.modelo.Document;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.CheckboxTreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Andres Agudelo
 */
@Named(value = "ttBasicView")
@ViewScoped
public class BasicView implements Serializable {

    /**
     * Creates a new instance of BasicView
     */
    private TreeNode root;
    
    private String name;
    private String size;
    private String type;
    

    private Document selectedDocument;

    public BasicView() {
        root = new DefaultTreeNode(new Document("Archivos", "-", "Carpeta"), null);
        init();
        createDocuments();
        mostrar();
        
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public TreeNode getRoot() {
        return root;
    }

    public Document getSelectedDocument() {
        return selectedDocument;
    }

    public void setSelectedDocument(Document selectedDocument) {
        this.selectedDocument = selectedDocument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    
    
    

    public TreeNode createDocuments() {

        TreeNode documents = new DefaultTreeNode(new Document("Documentos", "-", "Carpeta"), root);
        TreeNode pictures = new DefaultTreeNode(new Document("Imagenes", "-", "Carpeta"), root);
        TreeNode movies = new DefaultTreeNode(new Document("Peliculas", "-", "Carpeta"), root);

        TreeNode work = new DefaultTreeNode(new Document("Trabajo", "-", "Carpeta"), documents);
        TreeNode primefaces = new DefaultTreeNode(new Document("PrimeFaces", "-", "Carpeta"), documents);

        //Documents
        TreeNode expenses = new DefaultTreeNode("Documento", new Document("Trabajo 1.doc", "30 KB", "Word Document"), work);
        TreeNode resume = new DefaultTreeNode("Documento", new Document("Trabajo 2.doc", "10 KB", "Word Document"), work);
        TreeNode refdoc = new DefaultTreeNode("Documento", new Document("RefDoc.pages", "40 KB", "Pages Document"), primefaces);

        //Pictures
        TreeNode barca = new DefaultTreeNode("Imagenes", new Document("barcelona.jpg", "30 KB", "JPEG Image"), pictures);
        TreeNode primelogo = new DefaultTreeNode("Imagenes", new Document("logo.jpg", "45 KB", "JPEG Image"), pictures);
        TreeNode optimus = new DefaultTreeNode("Imagenes", new Document("optimusprime.png", "96 KB", "PNG Image"), pictures);

        //Movies
        TreeNode pacino = new DefaultTreeNode(new Document("Andres", "-", "Folder"), movies);
        TreeNode deniro = new DefaultTreeNode(new Document("Manolo", "-", "Folder"), movies);
        TreeNode Mateo = new DefaultTreeNode(new Document("Mateo", "-", "Folder"), movies);

        TreeNode scarface = new DefaultTreeNode("mp3", new Document("Scarface", "15 GB", "Movie File"), pacino);
        TreeNode carlitosWay = new DefaultTreeNode("mp3", new Document("Carlitos' Way", "24 GB", "Movie File"), pacino);

        TreeNode goodfellas = new DefaultTreeNode("mp3", new Document("Goodfellas", "23 GB", "Movie File"), deniro);
        TreeNode untouchables = new DefaultTreeNode("mp3", new Document("Untouchables", "17 GB", "Movie File"), deniro);

        return root;
    }

    public TreeNode createCheckboxDocuments() {

        TreeNode documents = new CheckboxTreeNode(new Document("Documents", "-", "Carpeta"), root);
        TreeNode pictures = new CheckboxTreeNode(new Document("Pictures", "-", "Carpeta"), root);
        TreeNode movies = new CheckboxTreeNode(new Document("Movies", "-", "Carpeta"), root);

        TreeNode work = new CheckboxTreeNode(new Document("Work", "-", "Folder"), documents);
        TreeNode primefaces = new CheckboxTreeNode(new Document("PrimeFaces", "-", "Folder"), documents);

        //Documents
        TreeNode expenses = new CheckboxTreeNode("document", new Document("Expenses.doc", "30 KB", "Word Document"), work);
        TreeNode resume = new CheckboxTreeNode("document", new Document("Resume.doc", "10 KB", "Word Document"), work);
        TreeNode refdoc = new CheckboxTreeNode("document", new Document("RefDoc.pages", "40 KB", "Pages Document"), primefaces);

        //Pictures
        TreeNode barca = new CheckboxTreeNode("picture", new Document("barcelona.jpg", "30 KB", "JPEG Image"), pictures);
        TreeNode primelogo = new CheckboxTreeNode("picture", new Document("logo.jpg", "45 KB", "JPEG Image"), pictures);
        TreeNode optimus = new CheckboxTreeNode("picture", new Document("optimusprime.png", "96 KB", "PNG Image"), pictures);

        //Movies
        TreeNode pacino = new CheckboxTreeNode(new Document("Al Pacino", "-", "Folder"), movies);
        TreeNode deniro = new CheckboxTreeNode(new Document("Robert De Niro", "-", "Folder"), movies);

        TreeNode scarface = new CheckboxTreeNode("mp3", new Document("Scarface", "15 GB", "Movie File"), pacino);
        TreeNode carlitosWay = new CheckboxTreeNode("mp3", new Document("Carlitos' Way", "24 GB", "Movie File"), pacino);

        TreeNode goodfellas = new CheckboxTreeNode("mp3", new Document("Goodfellas", "23 GB", "Movie File"), deniro);
        TreeNode untouchables = new CheckboxTreeNode("mp3", new Document("Untouchables", "17 GB", "Movie File"), deniro);

        return root;
    }

    public void mostrar() {
        System.out.println("root " + root.getData().toString());
        System.out.println(root.getChildren().get(0).getData().toString());
        
    }
    
    
    private List<SelectItem> categories;    
    private String selection;
 
    
    public void init() {
        categories = new ArrayList<SelectItem>();
        SelectItemGroup group1 = new SelectItemGroup("Carpeta");
        SelectItemGroup group2 = new SelectItemGroup("Documento");
        SelectItemGroup group3 = new SelectItemGroup("Video");
        SelectItemGroup group4 = new SelectItemGroup("Imagen");
        
         
        SelectItemGroup group21 = new SelectItemGroup("Word");
        SelectItemGroup group22 = new SelectItemGroup("Excel");
        SelectItemGroup group23 = new SelectItemGroup("Power Point");
        SelectItemGroup group24 = new SelectItemGroup("Archivo comprimido");
        
         
        SelectItem option31 = new SelectItem("mp4", "mp4");
        SelectItem option32 = new SelectItem("Avi", "Avi");
        SelectItem option33 = new SelectItem("Mov", "Mov");
        SelectItem option34 = new SelectItem("Wma Video", "Wma Video");
         
        SelectItem option41 = new SelectItem("jpg", "jpg");
        SelectItem option42 = new SelectItem("png", "png");
        SelectItem option43 = new SelectItem("svg", "svg");
        
         
     
        group2.setSelectItems(new SelectItem[]{group21, group22, group23, group24});
        group3.setSelectItems(new SelectItem[]{option31, option32, option33, option34});
        group4.setSelectItems(new SelectItem[]{option41, option42, option43 });
         
        categories.add(group1);
        categories.add(group2);
        categories.add(group3);
        categories.add(group4);
    }
     
    public List<SelectItem> getCategories() {
        return categories;
    }    
 
    public String getSelection() {
        return selection;
    }
    public void setSelection(String selection) {
        this.selection = selection;
    }
    
    
    public void createDocument(){
        if (selection == "carpeta"){
            TreeNode documents = new CheckboxTreeNode(new Document(name, "-", selection), root);
        }else{
             TreeNode documents = new CheckboxTreeNode(new Document(name, "-", selection), root);
        }
    }

}
